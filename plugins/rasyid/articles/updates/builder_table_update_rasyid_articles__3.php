<?php namespace Rasyid\Articles\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRasyidArticles3 extends Migration
{
    public function up()
    {
        Schema::table('rasyid_articles_', function($table)
        {
            $table->text('content')->default(null)->change();
            $table->timestamp('created_at')->default(null)->change();
            $table->timestamp('updated_at')->default(null)->change();
            $table->integer('year')->default(null)->change();
            $table->dropColumn('slug');
        });
    }
    
    public function down()
    {
        Schema::table('rasyid_articles_', function($table)
        {
            $table->text('content')->default('NULL')->change();
            $table->timestamp('created_at')->default('NULL')->change();
            $table->timestamp('updated_at')->default('NULL')->change();
            $table->integer('year')->default(NULL)->change();
            $table->string('slug', 191)->nullable()->default('NULL');
        });
    }
}
