<?php namespace Rasyid\Articles\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRasyidArticles extends Migration
{
    public function up()
    {
        Schema::table('rasyid_articles_', function($table)
        {
            $table->integer('year')->nullable();
            $table->text('content')->default(null)->change();
            $table->timestamp('created_at')->default(null)->change();
            $table->timestamp('updated_at')->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('rasyid_articles_', function($table)
        {
            $table->dropColumn('year');
            $table->text('content')->default('NULL')->change();
            $table->timestamp('created_at')->default('NULL')->change();
            $table->timestamp('updated_at')->default('NULL')->change();
        });
    }
}
