<?php namespace Rasyid\Articles\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRasyidArticles4 extends Migration
{
    public function up()
    {
        Schema::table('rasyid_articles_', function($table)
        {
            $table->integer('sort_order')->nullable();
            $table->text('content')->default(null)->change();
            $table->timestamp('created_at')->default(null)->change();
            $table->timestamp('updated_at')->default(null)->change();
            $table->integer('year')->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('rasyid_articles_', function($table)
        {
            $table->dropColumn('sort_order');
            $table->text('content')->default('NULL')->change();
            $table->timestamp('created_at')->default('NULL')->change();
            $table->timestamp('updated_at')->default('NULL')->change();
            $table->integer('year')->default(NULL)->change();
        });
    }
}
