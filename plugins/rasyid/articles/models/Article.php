<?php namespace Rasyid\Articles\Models;

use Model;

/**
 * Model
 */
class Article extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sortable; 

    /**
     * @var string The database table used by the model.
     */
    public $table = 'rasyid_articles_';
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];


    /**
     * @var array Validation rules
     */
    public $rules = [
        'title' => 'required',
        'content' => 'required',
        'year' => 'required'
    ];

    public $translatable = [
        'title',
        'content',
    ];
    /** Reorder List **/
    public function beforeValidate()
    {
        if (empty($this->sort_order))
        {
            $this->sort_order = static::max('sort_order') + 1;
        }
    }
}
