<?php return [
    'plugin' => [
        'name' => 'Menu Changer',
        'description' => 'Change menu name and icons easy',
    ],
    'settings' => [
        'menu' => 'Menu list',
        'menu_item' => 'Menu item',
        'menu_item_description' => 'Select the menu item you want to change',
        'menu_new_name' => 'Menu item new name',
        'menu_new_name_description' => 'Change menu item name',
        'menu_svg' => 'Menu item new image icon url',
        'menu_default_description' => 'Leave blank to use the default icon',
        'menu_font' => 'Menu item class attribute. Ex. https://octobercms.com/docs/ui/example/icon',
        'add_menu_item' => 'Add menu item to change'
    ]
];