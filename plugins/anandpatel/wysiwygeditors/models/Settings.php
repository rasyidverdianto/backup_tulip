<?php namespace AnandPatel\WysiwygEditors\models;

use Model;

class Settings extends Model
{
    
    public $translatable = [
        'content'
    ];
    
    public $implement = ['System.Behaviors.SettingsModel','@RainLab.Translate.Behaviors.TranslatableModel'];

    public $settingsCode = 'anandpatel_wysiwygeditors_settings';

    public $settingsFields = 'fields.yaml';

    protected $cache = [];

}
